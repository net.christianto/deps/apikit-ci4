# API Kit for CodeIgniter 4

Helps make the code cleaner by allowing for throwing ApiException as an early
exit.

This package, which I call a "kit", helps improve code cleanliness by allowing
for the early exit of code through throwing an ApiException. In CodeIgniter 4.4
(which has not been released as of now), the user-defined exception handler
applies to the entire project, making it problematic for routes that handle API
requests.

When viewed in a browser, these error presented as HTML, although it serve as
JSON under XHTML Request, we still want it in JSON format when viewed in
browser. Another problem is, the returned fields are inconsistent from
`ResponseTraits` from `API` namespace.

The "kit" makes code cleaner by providing an easy way to use `ApiException` as
an early exit, improving the handling of exceptions in API routes in CodeIgniter
4.4 where the user-defined exception handler applies to the entire project.

## Usage
1. Install it via Composer

    ```bash
    composer require net.christianto/apikit-ci4
    ```

2. Add the filter to your API Routes. This will catch all of your thrown API
   Exceptions.

    ```php
    // app/Config/Routes.php

    $routes->get('/-/ping', 'Api\Pinger::index', ['filter' => 'api_exception_catch']);
    ```

3. In your controller, just throw any `ApiException` when you want to report
   errors. `ApiExceptionCatcher` will automatically serve your error with your
   corresponding error message to screen properly.

    ```php
    // app/Controllers/Api/Pinger.php

    namespace App\Controllers\Api;

    use App\Controllers\BaseController;
    use CHEZ14\ApiKit\Exceptions\ApiException;
    use CodeIgniter\API\ResponseTrait;

    class Pinger extends BaseController
    {
        use ResponseTrait;

        public function index()
        {
            $request = request();

            if ($request->getGet('id') != '405') {
                throw new ApiException("I cannot understand that.", 400);
            }

            return $this->respond([
                "status" => true,
                "message" => "I'm 99!"
            ], 200);
        }
    }
    ```

Please note that we only catch instances of `ApiException` Exceptions only. Any
other exception will be thrown to the framework.

For example, please check following snippets:

```php
public function index()
{
    $request = request();

    if ($request->getGet('id') != '405') {
        throw new InvalidArgumentException("I cannot understand that.", 400);
    }

    return $this->respond([
        "status" => true,
        "message" => "I'm 99!"
    ], 200);
}
```

`InvalidArgumentException` will not be catched by `api_exception_catch` filter
because it is not an instance of `ApiException`. So it will be forwarded stright
to the frarmwork's default exception handler, whatever it is.

I believe that treating certain exception as a valid, intentional API result,
should be opt-in only. Catching all exception and reformat them as API result is
not desireable because we can't differentiate if this error are the intended
logic or the PHP telling us that we're doing something wrong.

## License

[MIT](./LICENSE).
