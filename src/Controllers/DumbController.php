<?php

namespace CHEZ14\ApiKit\Controllers;

use App\Controllers\BaseController;
use CHEZ14\ApiKit\Exceptions\ApiException;
use CodeIgniter\API\ResponseTrait;

class DumbController extends BaseController
{
    use ResponseTrait;

    /**
     * Helps filter to throw HTTP Errors by API Exception
     *
     * @param ApiException $e The exception needs to be presented
     * @return void
     */
    public function throwError(ApiException $e)
    {
        $data = [
            "status" => false,
            "message" => $e->getMessage(),
            "data" => null
        ];

        // TODO: Throw more information when we're in Dev mode;

        $this->respond($data, $e->getHttpCode())->send();
    }
}
