<?php

namespace CHEZ14\ApiKit\Filters;

use CHEZ14\ApiKit\Controllers\DumbController;
use CHEZ14\ApiKit\Exceptions\ApiException;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use Config\Services;
use Throwable;

class ApiExceptionCatcher implements FilterInterface
{
    /**
     * Set the previous exception handler to forward this to.
     *
     * @var callable<Throwable>
     */
    protected $previousExceptionHandler;

    /**
     * Help handle exception thrown by the controller. This will help handle the
     * ApiException thrown by controller, but specificly just for that
     * exception. Any other exception will be forwarded to next handler.
     *
     * @param Throwable $e The provided exception
     * @return void
     */
    public function handleApiException(Throwable $e)
    {
        if (!$e || !($e instanceof ApiException)) {
            return call_user_func($this->previousExceptionHandler, $e);
        }

        // Change the Response Interface
        $controller = new DumbController();
        $controller->initController(Services::request(), Services::response(), Services::logger());
        $controller->throwError($e);
    }

    /**
     * Do whatever processing this filter needs to do. By default it should not
     * return anything during normal execution. However, when an abnormal state
     * is found, it should return an instance of CodeIgniter\HTTP\Response. If
     * it does, script execution will end and that Response will be sent back to
     * the client, allowing for error pages, redirects, etc.
     *
     * @param RequestInterface $request Original Request
     * @param array|null $arguments Arguments for this filter
     * @return mixed
     */
    public function before(RequestInterface $request, $arguments = null)
    {
        // Register the API Handler thingy. This will help accept.
        $this->previousExceptionHandler = set_exception_handler([$this, 'handleApiException']);
    }

    /**
     * Allows After filters to inspect and modify the response object as needed.
     * This method does not allow any way to stop execution of other after
     * filters, short of throwing an Exception or Error.
     *
     * @param RequestInterface $request Original Request
     * @param ResponseInterface $response Original Response
     * @param array|null $arguments Arguments for this filter
     * @return mixed
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Restore the Exception handler
        restore_exception_handler();
    }
}
