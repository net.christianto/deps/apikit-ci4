<?php

namespace CHEZ14\ApiKit\Exceptions;

use CodeIgniter\Exceptions\HTTPExceptionInterface;
use LogicException;

class ApiException extends LogicException implements HTTPExceptionInterface
{
    protected $httpCode = 500;

    /**
     * Return the HTTP Code thrown by this little dude.
     *
     * @return integer
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    /**
     * @param string $message Error message, will be set to `message` part.
     * @param integer $httpCode HTTP Code to be sent to the client.
     * @param integer $exceptionCode Exception code. Helpful for debugging things.
     */
    public function __construct(string $message, int $httpCode = 500, int $exceptionCode = 0)
    {
        parent::__construct($message, $exceptionCode);
        $this->httpCode = $httpCode;
    }
}
