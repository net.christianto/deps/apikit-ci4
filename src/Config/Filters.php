<?php

namespace CHEZ14\ApiKit\Config;

use CHEZ14\ApiKit\Filters\ApiExceptionCatcher;
use Config\Filters;

/**
 * @var Filters $filters
 */
$filters->aliases['api_exception_catch'] = ApiExceptionCatcher::class;
